#include <iostream>
#include <string>
#include <algorithm>
#include <iterator>
#include <ctime>
using namespace std;        // creating header

string checkAnswer(char userAnswer[4], char computerAnswer[4]){
    /* creating function that takes in the correct color sequence and player-guessed sequence
     * which are two arrays of char and provide feedback of how similar are the two sequences in a string */

    char ans;        // create an empty char which will be used as each char of the player-guessed sequence
    string feedback = "";       // create an empty string which will become the feedback string returned by the function

    for (int x = 0; x < 4; x++){        // loop through each color in the player-guessed color sequence
        ans = userAnswer[x];

        if (find(computerAnswer, computerAnswer+5, ans) != computerAnswer+5){       // if this color is in the sequence
            // of correct colors
            if (computerAnswer[x]==ans){        // if this color is at the right position
                feedback = feedback + "(" + ans + "X) ";       // add the color name with the black check in the feedback
            }
            else {      // if the color is in the sequnce but not in the right position
                feedback = feedback  + "(" +  ans + "O) ";       // add the color name with the white check in the feedback
            }
        }
        else{       // if the color is not in the sequence at all
            feedback = feedback  + "(" +  ans + "_) ";       // add the color name with nothing
        }
    }

    return  feedback;        // return the string of colors with the score
}

void answer_func (char* answer_arry){
    int answer[4];
    srand(int(time(NULL)));     // this is used generate random numbers

    char blk = 'k';
    char blu = 'b';
    char whi = 'w';
    char gre = 'g';
    char red = 'r';
    char yel = 'y';


    for (int i = 0; i < 4; i++){     // Use a for loop to create the array.

        int random_numbers= rand() % 6;  // This will make sure the numbers stay between 1 and 6.

        switch (random_numbers) {
            case 0:
                answer_arry[i] = blk;
                break;
            case 1:
                answer_arry[i] = blu;
                break;
            case 2:
                answer_arry[i]  = whi;
                break;
            case 3:
                answer_arry[i]  = gre;
                break;
            case 4:
                answer_arry[i] = red;
                break;
            case 5:
                answer_arry[i]  = yel;
                break;
            default:
                break;
        }
    }


    cout << "\n";


}


void usr_guess (char* usr_ans) {
    //Variables used
    int guessing = 4; //while user is still guessing
    int place = 0; //place where guess will go, incremented in program
    string guess_str; //user guess single color
    char guess;


    while (guessing > 0)
    {
        //asking for and getting answer, changing it to lower case
        cout << "Color: ";
        cin >> guess_str;

        if (guess_str.length() != 1) {
            cout << "Please only give one letter." << endl;
        }
        else {
            guess = guess_str[0];
            guess = tolower(guess);//changes user answer to lowercase for handling


            //checks if user inputs matches a color
            switch (guess) {
                case 'r'://red
                    usr_ans[place] = guess;//adds to user guess array
                    place += 1; //changes position in array for next color added
                    guessing -= 1;//counter for how many guesses can be made
                    break;

                case 'y'://yellow
                    usr_ans[place] = guess;
                    place += 1;
                    guessing -= 1;
                    break;

                case 'g'://green
                    usr_ans[place] = guess;
                    place += 1;
                    guessing -= 1;
                    break;

                case 'b'://blue
                    usr_ans[place] = guess;
                    place += 1;
                    guessing -= 1;
                    break;

                case 'w'://white
                    usr_ans[place] = guess;
                    place += 1;
                    guessing -= 1;
                    break;

                case 'k'://black
                    usr_ans[place] = guess;
                    place += 1;
                    guessing -= 1;
                    break;

                default://wrong input ask again
                    cout << "\n" << "The answer given is not valid, please enter another color." << endl;
            }

        }
    }

    cout << "\n";//spacing

    return;
}



int main() {
    cout << "Welcome to Mastermind" << endl;        //print instruction
    cout << "In this game you will try and guess the order of four pegs by their color" << endl;
    cout << "the colors you will use are black, blue, white, green, red, and yellow" << endl;
    cout << "when inputing values please use the charaters given" << endl;
    cout << "black = k, red = r, green = g, white = w, blue = b, yellow = y" << endl;
    cout << "you will get eight tries, are you ready?" << endl;


    string gameBoard;       // create empty gameboard

    char answer[4]; // 0-3 will be the size of the array.
    char *answer_ptr = answer; //pointer to computer answer
    answer_func(answer_ptr);//calls function to create random array of colors

    char usr_answer[4] = {};//user guess array
    char *usr_ans_ptr = usr_answer;//pointer to user guess

    int guess_count = 0;

    for (; guess_count < 8; guess_count++) {


        usr_guess(usr_ans_ptr);//calls function to ask user for color guesses
        gameBoard = gameBoard + checkAnswer(usr_answer, answer) + "\n";    //put each result of the round onto gameboard
        cout << gameBoard << endl;      // display the gameboard for user to see

        if (equal(begin(answer), end(answer), begin(usr_answer))){      // check if the user win the game
            cout << "Congradulations! you won the game!" << endl;
            break;
        }

    }

    if (not equal(begin(answer), end(answer), begin(usr_answer))) {     //if user ran out of step, they lose
        cout << "Sorry, you've ran out of the steps. You lost." << endl;
        cout << "If you want to play, please re-run the programr" << endl;
    }
    return 0;
}